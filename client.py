#! /usr/bin/python

import socket
import sys
from random import randint
import thread
import readline
from colorama import *
import signal
import struct, fcntl, termios
from time import sleep
from crypy import CryPy


host = '127.0.0.1'
port = 5050	

cryptor = CryPy()

def close_connection(signal, frame):
	# exit_program()
	raise EOFError

signal.signal(signal.SIGINT, close_connection)

def blank_current_readline():
    # Next line said to be reasonably portable for various Unixes
    (rows,cols) = struct.unpack('hh', fcntl.ioctl(sys.stdout, termios.TIOCGWINSZ,'1234'))

    text_len = len(readline.get_line_buffer())+2

    # ANSI escape sequences (All VT100 except ESC[0G)
    sys.stdout.write('\x1b[2K')                         # Clear current line
    sys.stdout.write('\x1b[1A\x1b[2K'*(text_len/cols))  # Move cursor up and clear line
    sys.stdout.write('\x1b[0G')                         # Move to start of line


def client_thread(client_socket):
	try:
		# Keep waiting for a message from the other user and print it
		while True:
			encrypted_data = client_socket.recv(4096)

			data = cryptor.decrypt(encrypted_data)

			if data:
				if data == 'exit_server':
					break
				else:
					blank_current_readline()
					sys.stdout.write(Fore.GREEN + "[ZIZO]> %s (%s)\n" % (data, encrypted_data))
					sys.stdout.write(Fore.BLUE + '[RAMY]> ' + readline.get_line_buffer())
					sys.stdout.flush()

	except Exception, e:
		print 'ERROR !!', e
		pass
	finally:
		# If the loop is broken or excption thrown in other user socket, close the connection
		print "%s:%s closed" % (host, port)
		client_socket.close()
		sys.exit()


def new_client(client_id):

	# Create a new socket object for the user (tcp socket)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print "client -> Client #%d connecting to %s:%s" % (client_id, host, port)

	try:
		# Connect to the server (localhost:5050)
		sock.connect((host, port))
		# Get the port and address of the current user socket
		socket_name = sock.getsockname()

		# Set key as port number padded by zeros
		my_port_number = str(socket_name[1])
		key = cryptor.make_multiple_of_8( my_port_number, '0', len(my_port_number) )
		cryptor.set_key( key )


		# Make a thread for receving messages from the other user
		thread.start_new_thread(client_thread, (sock, ))
		# Prompt the user for input message & send it
		input_message = Fore.BLUE + "[RAMY]> " 
		message = raw_input(input_message)
		# While message is other than exit keep prompting the user for input
		while message != 'exit':
			# Send the message to the other client
			encyrpted_message = cryptor.encrypt(message)
			sock.sendall(encyrpted_message)
			# Re-prompt the user
			message = raw_input(input_message)
	except EOFError:
			pass
	except Exception, e:
		print 'CLIENT: ERROR !!', e
		pass
		# print "client -> Closing Connection for client with exception #", client_id, e
	finally:
		# if exception is thrown or user typed exit, close the socket of the user
		print "client -> Closing Connection #%d." % (client_id)
		sock.close()

# Let's begin
if __name__ == '__main__':
	init() # for coloroma (windows)
	new_client(randint(0, 9))
