import socket
import sys
import thread
import signal
import errno
import readline
from colorama import *
import struct, fcntl, termios
from time import sleep
from crypy import CryPy

EXIT_FLAG = False
host = '127.0.0.1'
port = 5050

cryptor = CryPy()

# Listen for CTRL+C signal
def close_server(signal, frame):
	exit_server()

signal.signal(signal.SIGINT, close_server)

def blank_current_readline():
    # Next line said to be reasonably portable for various Unixes
    (rows,cols) = struct.unpack('hh', fcntl.ioctl(sys.stdout, termios.TIOCGWINSZ,'1234'))

    text_len = len(readline.get_line_buffer())+2

    # ANSI escape sequences (All VT100 except ESC[0G)
    sys.stdout.write('\x1b[2K')                         # Clear current line
    sys.stdout.write('\x1b[1A\x1b[2K'*(text_len/cols))  # Move cursor up and clear line
    sys.stdout.write('\x1b[0G')                         # Move to start of line



def client_thread(client_socket, client_address):
	try:
		# print "\nNew connection from %s:%s " % (client_address[0], client_address[1])
		# Wait for a message from the client
		while True:
			encrypted_data = client_socket.recv(4096)

			data = cryptor.decrypt(encrypted_data)

			if data:
				blank_current_readline()
				sys.stdout.write(Fore.BLUE + "[RAMY]> %s (%s)\n" % (data, encrypted_data))
				sys.stdout.write(Fore.GREEN + '[ZIZO]> ' + readline.get_line_buffer())
				sys.stdout.flush()

	except Exception, e:
		print 'ERROR !!', e
		pass
	finally:
		# if exception is thrown or loop broken or the socket of client throw an expcetion close the socket of the client
		print "%s:%s closed" % (client_address[0], client_address[1])
		client_socket.close()
		sys.exit()



def main():
	# Create a socket object (TCP)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# Allow for re-using the port & host (re use address)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	print "Starting Server at %s:%s" % (host, port)
	# Bind the server socket to host:port (localhost:5050)
	sock.bind((host, port))

	# Allow up to 2 sockets connections
	sock.listen(2)

	# Wait for a connection from client
	while True:
		# Close the server if exit flag is raisd by CTRL+C
		if EXIT_FLAG:
			break

		# print "Waiting for a connection"
		try:
			# Accept A new client connection
			client_socket, client_address = sock.accept() 

			# GENERATE A KEY FROM PORT NUMBER AND SEND IT TO CLIENT
			client_port_number = str(client_address[1])
			key = cryptor.make_multiple_of_8( client_port_number, '0', len(client_port_number) )
			cryptor.set_key( key )

			# Create a thread for the client to handle receving messages from him
			thread.start_new_thread(client_thread, (client_socket, client_address)) 
			
			# Prmopt for message & send it to the client  
			input_message = Fore.GREEN + '[ZIZO]> '
			message = raw_input(input_message)
			# While message is other than exit keep prompting the user for input
			while message != 'exit':
				# Send the message to the other client
				encrypted_message = cryptor.encrypt(message)
				client_socket.sendall(encrypted_message)
				# Re-prompt the user
				message = raw_input(input_message)
			break
		except EOFError:
			break
		except Exception, e:
			print 'SERVER: ERRORR !!', e
			pass
		except socket.error, e:
			# Check if the error related to socket
			if e.errno != errno.EINTR:
				raise
		finally:
			try:
				client_socket.sendall(cryptor.encrypt('exit_server'))
			except Exception, e:
				pass
			finally:
				pass
				

	# Close the server socket connection
	print "closed server connection"
	sock.close()
		

# Handle method of CTRL+C to close the server
def exit_server():
	global EXIT_FLAG
	EXIT_FLAG = True


# Let's begin
if __name__ == '__main__':
	init() # for coloroma (windows)
	main()
