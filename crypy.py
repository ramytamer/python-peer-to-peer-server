from Crypto.Cipher import DES
import string, random

class CryPy():

	def __init__(self, key=None):
		if key:
			self.set_key(key)
		else:
			self.cry_des = None
		

	def set_key(self, key):
		self.cry_des =  DES.new(key, DES.MODE_ECB)

	def encrypt(self, plain_text):
		if self.cry_des == None:
			raise ValueError('Unspecified key')
		plain_text = self.make_multiple_of_8(plain_text, ' ', len(plain_text))
		return self.cry_des.encrypt(plain_text)

	def decrypt(self, message):
		if self.cry_des == None:
			raise ValueError('Unspecified key')
		return self.cry_des.decrypt(message)

	# Generate random key with size multiple of 8
	def generate_key(self, size=24):
		size = self.make_multiple_of_8(size, 1, size)
		random_domain = string.ascii_uppercase + string.ascii_lowercase + string.digits
		key = ''.join(random.choice(random_domain) for _ in range(size))
		self.set_key(key)
		return key

	# Function to make any object a length of multiple of mod_number (by default 8)
	def make_multiple_of_8(self, start, append, size, mod_number=8):
		if size % mod_number != 0:
			for x in range(0, ( mod_number - (size % mod_number) ) ):
				start += append
		return start
